# Computación, Software e Ingeniería

La ingeniería de software es un área puntual dentro de lo que el imaginario cultural denomina en forma general como computación. Comencemos entonces hablando un poco sobre ello.


## Evolución de las computadoras

Algunos citan al ábaco como la primera computadora, lo ubicaría el comienzo de la computación varios siglos atrás. Pero en lo que a nuestro estudio compete no nos remontaremos tan atrás. Para nuestros fines nos basta con remontarnos a mediados del siglo XX, con la aparición de los primero computadores electrónicos y los fundamentales aporte de Alan Turin y John Von Neumann. 

En aquellos primeros años las computadoras eran realmente grandes, ocuparán habitaciones enteras. Su uso era principalmente para la realización de cálculos y eran operadas por científicos, principalmente matemáticos. Sólo había computadoras en algunas universidades, centros de investigación y dependencias militares.

En las décadas de 1960 y 1970 las computadoras llegan al sector privado y más organismo gubernamentales. Aún en aquella época cada fabricante de computadoras proveía el hardware junto con su propio sistema operativo. Recién a parti de la aparición de UNIX en los 70', los distintos fabricantes comenzaron a proveer sistemas operativos más uniformes derivados de UNIX. Una computadora ícono de ese período es la IBM System/360.

Durante los 70' empiezan a difundirse las computadoras de escritorio y a comienzos de los 80' se inaugura una nueva era a partir de la aparición de la computadora personal de IBM (IBM Personal Computer) una máquina de escritorio accesible para la población en general. Durante los 80' las computadoras personales comienzan a llegan a los hogares y pequeñas empresas. "La computación" se introduce en los programas de estudio de la educación básica con aplicaciones como Logo y BASIC.

Ya hacia los 90', el siguiente gran hito fue aparición de internet. Al mismo tiempo el uso de computadoras personales (cada vez más accesibles económicamente) fue en constante aumento. 

Durante la primera década del nuevo milenio se masifica el acceso a internet a partir de conexiones de banda ancha. Las computadoras de escritorio comienzan a divisar un nuevo competidor: las computadoras portátiles.

Finalmente llegamos a la situación actual (2015): la omnipresencia de la nube, la invasión de dispositivos móviles y las "apps", la internet de las cosas, netfix, whatups, etc, etc.

## ¿De qué hablamos cuando decimos Computación?

Al hablar de computación sin ser estrictos y términos generales podemos distinguir 2 grandes incumbencias: Hardware y Software.

El hardware refiere a la parte física de la computación, en concreto: los dispositivos. Ejemplos de dispositivos son: servidores, computadoras de escritorio, computadoras portátiles, tabletas, teléfonos, monitores, teclados, etc. 

Pero el hardware por si solo es poco lo que puede ser. El hardware requiere del software para resultarnos útil. El software es la parte "no física" de la computación: los programas, las "apps" como se las suele llamar al hablar de dispositivos móviles. En términos generales el software puede clasificarse en 3 grandes grupos:

* Firmware, es el software que viene directamente embebido en el hardware y cuya actualización es muy esporádica.
* Software de base, constituido por los sistemas operativos y otros programas que agregan una primera capa de funcionalidad genérica al hardware.
* Software aplicativo, permite utilizar el hardware para funciones específicas como ser jugar, editar texto, manipular imágenes, etc, etc.


Nota: en la actualidad los sistemas operativos suelen venir con algunos aplicativos de software como paquetes de oficina.

Más allá de la clasificación de software citada previamente, resulta interesante la clasificación propuesta por Fred Brooks quien propone analizar el software en dos dimensiones:

* Integración
* Prueba-Documentación-Mantenimiento

## Características del software
La construcción de software es una actividad compleja en gran medida por ciertas particularidades del software:

* Intangible
* Maleable
* "Idea" de que los cambios son fáciles
* Es "humano-intensivo", alto contenido intelectual
* Alto costo fijo
* Potencialmente modificable infinitamente
* Construcción basado en equipo y por proyecto


En sintonía con este ultimo punto, Brooks nos ha relegado un conjunto de ensayos en los cuales se destacan algunas particularidades de los proyectos de software:

* El mítico hombre-mes
* No silver bullet
* Complejidad esencial y accidental


## La computación en la sociedad

Inicialmente la computación comenzó extendiéndose en organizaciones y con el correr del tiempo se fue introduciendo en el día a día de toda la sociedad. De una u otra manera, la gran mayoría de la población interactúa hoy por hoy con al menos una computadora todos los días de su vida. Más allá de la concepción tradicional de computadora, tenemos en la actualidad computadoras en diversos artefactos: teléfonos, autos, televisores, electrodomésticos, etc. Todas esas computadores tienen software que es construido por organizaciones: empresas públicas o privadas, universidades, institutos, cooperativas, etc.
Al mismo tiempo tenemos que prácticamente todas las organizaciones utilizan computadoras ya sea se dediquen al software como actividad principal o no. Por esto es común que muchas organizaciones cuentes con sectores encargados de la computación, los cuales suelen recibir distintos nombres dependiendo principalmente de sus tareas y responsabilidades. En este sentido es común encontrarse con áreas/sectores/gerencias denominadas como:

* Tecnología de la información: es la denominación más general, utilizada muchas veces en organizaciones medianas/grandes. Justamente organizaciones medianas/grandes, suele tener sub áreas.
* Sistemas Informáticos: es el nombre utilizado muchas veces para referirse a sectores con foco en cuestiones de software.
* Soporte técnico: su foco suele ser cuestiones de hardware incluyen redes, impresoras y diversos dispositivos. En común que en organizaciones de cierto tamaño sea un subarea de sistemas o tecnología.

## El rol de TI en las organizaciones
Independiente del nombre que reciba el área TI en las organizaciones, resulta relevante para nosotros como profesional informáticos analizar el rol que juega TI en las organizaciones.

* Facilitador de los procesos de negocio. En estos casos el negocio de la organización existía incluso antes de la aparición de la computación y por ello es que la organización podría seguir operando incluso sin computadoras. Un caso típico de esto es retail: si bien hoy en día muchos supermercados (sobre todo los de las grandes cadenas) tienen importantes área de sistemas, hay todavía supermercados que funcionan con apenas una caja registradora que es poco más que una calculadora.
* Transformador de los procesos de negocios: si bien el negocio existía incluso antes de las computadoras, es tal el impacto que TI ha tenido en el negocio que sus procesos han sido completamente modificados a punto tal que hoy por hoy estas organizaciones casi que no podrían operar sin TI. Un caso típico de este rol de TI se observa en organizaciones como bancos.
* Posibilitador del negocio: en estos casos, el negocio no existía antes de las computadoras. En este caso entran todas las empresas dedicadas a la construcción de software pero también algunas otras como Facebook, Twitter y Spotify.

Esta clasificación es muy general y sin duda dar lugar a muchos grises: ¿En que grupo estarían las siguientes organizaciones?:

* Despegar.com
* Mercado Libre
* Trip Advisor
* AFIP
* Universidades
* Coursera
 
A pesar de sus limitaciones, esta clasificación nos permite identificar cómo trabajará cada tipo de organización en determinadas situaciones. Teniendo esto presente te invitamos a que reflexiones sobre las siguientes preguntas:

* ¿En que tipo de organización crees que los profesionales informáticos tendrán más posibilidades de capacitación?
* ¿Que tipo de organización crees que estará más dispuesta a experimentar con nuevas tecnologias?
* ¿Que tipo de organización crees que tendrá mayor cantidad de regulaciones?

## Ingeniería de Software
Hasta el momento venimos hablando de computación y software, nos toca ahora adentramos en el tema de estudio de este curso, la ingeniería de software. Comencemos entonces por la definición de ingeniería:

_Es la ciencia y el arte de utilizar las fuerzas y los elementos de la naturaleza para el mejor servicio al hombre y la sociedad._

Dicho esto podemos definir la ingeniería de software como:

_La rama de la ingeniería que crea y mantiene las aplicaciones de software aplicando tecnologías y prácticas de las ciencias computacionales, manejo de proyectos, ingeniería, el ámbito de la aplicación, y otros campos._

Más allá de la definición formal hay algunos puntos interesantes de la ingeniería de software que la distinguen de las otras ingeniarías:

* Es una disciplina reciente, surgida a mediados del siglo XX. Uno de los primeros registros del término "Ingeniería de software" data de una conferencia de la [OTAN del año 1968](http://homepages.cs.ncl.ac.uk/brian.randell/NATO/NATOReports/).
* Las herramientas de construcción son también un producto, o sea, el software se construye usando software
* Nuevas tecnologías aparecen a todo el tiempo
* Todo software es es parte de un sistema más amplio (no software is an island)
* Existe una gran reutilización de experiencias
* El software es conocimiento empaquetado. Cuando construimos un software para calculo de impuestos estamos embebiendo en el software conocimiento para realizar dichos cálculos. Esa propiedad del software no es gratuita, la misma es consecuencia de los conocimientos necesarios para construir el software: Conocimiento de ingeniería, Conocimiento tecnológico y Conocimiento de negocio. 

## Actividades

1. Leer el artículo "No silver Bullet" de Fred Brooks
2. Leer el artículo "Software Engineering missing in action" de David Parnas
3. En toda actividad existen organismos de regulación y difusión. En el caso del fútbol tenemos la FIFA, la AFA, Futbolistas Agremiados, etc. La computación, el software y la ingeniería no son una excepción, por eso te invitamos a que investigues los siguientes organismos: IEEE, ACM, SADIO, Agile Alliance, SEI-CMU.
4. Además de organismos toda actividad cuenta con ciertas personalidades destacadas que han hecho algún aporte de valor a la actividad. En el fútbol podríamos mencionar a Maradona, Pelé, Platini o Messi. Te invitamos a que investigues los aportes realizados por cada uno de los siguientes individuos: Edsger Dijkstra, Donal Knuth, Alain Turin, Alan Key, Barbara Liskov, Fred Brooks, Tom De Marco, Edward Demming, Los tres amigos, GoF, Kernighan Ritchie, David Garlan, Bertrand Meyer, Kent Beck y Robert Martin.