# Modelo de ciclo vida

## Actividades en el desarrollo de software

La construcción de software implica naturalmente la realización de ciertas actividades: 

* Análisis: entender qué vamos a construir
* Diseño: definir cómo lo vamos a construir
* Programación: que equivale a construcción propiamente dicha
* Prueba: verificar que lo construido cumple con lo esperado
* Puesta en marcha: es la entrega del producto para que lo usen sus destinatarios

El orden en que se ejecutan estas actividades, la forma en que se encuentras agrupadas y el hecho de cuán explícitamente están definidas, determinan lo que denominamos modelo de ciclo de vida o modelo de proceso.

Dicho modelo tiene un impacto directo en diversos aspectos del proyecto de desarrollo de software como ser:

* La planificación, ejecución y control
* El manejo de expectativas y riesgos
* El flujo de entrega de valor

## Modelo code and fix
TODO: este modelo es comúnmente utilizado por aquellas personas y organizaciones que suelen trabajar al estilo "cowboy programming".

![Modelo Code & Fix](imagenes/modelo_code_and_fix.png)

## Modelo en Cascada (waterfall)

TODO

![Modelo Cascada](imagenes/modelo_cascada.png)

## Modelo Prototipado

TODO

## Modelo en espiral

TODO

## Modelo iterativo incremental

TODO

![Modelo Iterativo Incremental](imagenes/modelo_iterativo_incremental.png)


## Modelo Ágil

TODO

![Modelo Ágil](imagenes/modelo_agil.png)



