# Y un día llegamos a producción: Operaciones

Bien, trabajamos de cerca con nuestros usuarios y finalmente tenemos un conjunto de funcionalidades listas para producción. ¿y ahora qué? bueno, estamos hablando de una aplicación web así que supongo que tomamos un servidor, instalamos y listo, a hacer dinero.

Alto ahí, hay algunas cuestiones adicionales que deberíamos tener presentes si pretendemos que nuestra vida no se torne tortuosa.

Si bien cada aplicación y escenario tiene sus particularidades hay ciertas cuestiones que suelen estar presentes en una gran cantidad de escenarios.

## Backup

Es común que una aplicación maneje información, ya sea esta provista por un usuario o bien recolectada/generada por la propia aplicación. En muchos contexto dicha información es un activo valioso para la organización y no se puede correr el riesgo de perderla. Es por esto que las organizaciones suelen implementar estrategias de backup (resguardo)

## Monitoreo

TODO

## Actualización

TODO

## Rollback

TODO

